package test.java.com.sohich.mtc;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import main.java.com.sohich.mtc.*;

import java.io.IOException;
import java.io.StringReader;

public class ParserTest
{
    @Test
    public void simpleCalcTest() throws IOException, ParseException, LexerException
    {
        Parser p = new Parser(new Lexer(new StringReader("2+2")));
        assertEquals(4, p.calculate());
    }

    @Test
    public void soMuchBracketsTest() throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("((((2+2))))")));
        assertEquals(4, p.calculate());
    }

    @Test(expectedExceptions = ParseException.class)
    public void wrongOpeningBracketTest() throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("(2*2")));
        assertEquals(4, p.calculate());
    }

    @Test(expectedExceptions =  ParseException.class)
    public void wrongClosingBracketTest()  throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("2/2)")));
        assertEquals(1, p.calculate());
    }

    @Test(expectedExceptions = ParseException.class)
    public void emptyExpressionTest()  throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("(())")));
        assertEquals(0, p.calculate());
    }

    @Test(expectedExceptions = ArithmeticException.class)
    public void divisionByZeroTest()  throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("2/(3-3)")));
        assertEquals(0, p.calculate());
    }

    @Test
    public void unaryMinusTest()  throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("-1-2*3")));
        assertEquals(-7, p.calculate());
    }

    @Test
    public void simpleDegreeTest()  throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("2^3")));
        assertEquals(8, p.calculate());
    }

    @Test
    public void multiDegreeTest()  throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("2^3^2")));
        assertEquals(1<<9, p.calculate());
    }

    @Test
    public void negativeDegreeTest()  throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("3^(4-5)")));
        assertEquals(0, p.calculate());
    }

    @Test
    public void zeroInZeroDegreeTest()  throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("(0^0)")));
        assertEquals(1, p.calculate());
    }

    @Test(expectedExceptions = ParseException.class)
    public void zeroInNegativeDegreeTest()  throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("0^(4-5)")));
        assertEquals(0, p.calculate());
    }

    @Test(expectedExceptions = ParseException.class)
    public void wrongTermTest() throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("2+")));
        p.calculate();
    }

    @Test(expectedExceptions = ParseException.class)
    public void wrongFactorTest() throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("2*")));
        p.calculate();
    }

    @Test(expectedExceptions = ParseException.class)
    public void wrongDegreeTest() throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("2^2^")));
        p.calculate();
    }

    @Test(expectedExceptions = ParseException.class)
    public void signTwiceTest() throws IOException, LexerException, ParseException
    {
        Parser p = new Parser(new Lexer(new StringReader("2^*")));
        p.calculate();
    }
}
