package test.java.com.sohich.mtc;

import org.testng.annotations.Test;
import main.java.com.sohich.mtc.*;

import java.io.IOException;
import java.io.StringReader;

import static org.testng.Assert.assertEquals;

public class LexerTest
{
    @Test
    public void trimTest()
    {
        Lexer l = new Lexer(new StringReader("    2  + 2"));
        Lexeme lexeme;
        String result = "";
        try {
            do {
                {
                    lexeme = l.getLexeme();
                    result += lexeme.getLexeme();
                }
            }
            while(lexeme.getType() != Lexeme.LexemeType.EOF);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        assertEquals("2+2", result);
    }

    @Test
    public void emptyInputTest() throws IOException, LexerException
    {
        Lexer l = new Lexer(new StringReader(""));
        assertEquals(Lexeme.LexemeType.EOF, l.getLexeme().getType());
    }

    @Test
    public void spacesAreNotIgnoredTest() throws IOException, LexerException
    {
        Lexer l = new Lexer(new StringReader("234 457"));
        Lexeme lexeme;
        int count = 0;
        do {
                lexeme = l.getLexeme();
                if(lexeme.getType() == Lexeme.LexemeType.NUMBER)
                {
                    ++count;
                }
        }
        while(lexeme.getType() != Lexeme.LexemeType.EOF);
        assertEquals(2, count);
    }

    @Test(expectedExceptions = LexerException.class)
    public void unknownSymbolTest() throws IOException, LexerException
    {
        Lexer l = new Lexer(new StringReader("2 + $"));
        Lexeme lexeme;
        do {
            lexeme = l.getLexeme();
        }
        while(lexeme.getType() != Lexeme.LexemeType.EOF);

    }

    @Test
    public void symbolsAreParsedCorrectly() throws IOException, LexerException
    {
        Lexer l = new Lexer(new StringReader("1234567890()+-*/^()"));
        String result = "";
        Lexeme lexeme;
        do {
            {
                lexeme = l.getLexeme();
                result += lexeme.getLexeme();
            }
        }
        while(lexeme.getType() != Lexeme.LexemeType.EOF);
        assertEquals("1234567890()+-*/^()", result);
    }
}
