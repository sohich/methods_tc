package main.java.com.sohich.mtc;

public class LexerException extends Exception
{
    public LexerException()
    {
        super();
    }

    public LexerException(String reason)
    {
        super(reason);
    }
}
