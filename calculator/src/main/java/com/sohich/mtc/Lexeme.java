package main.java.com.sohich.mtc;

public class Lexeme
{
    private String lexeme;
    private LexemeType type;

    public Lexeme(String lexeme, LexemeType type)
    {
        this.lexeme = lexeme;
        this.type = type;
    }

    public String getLexeme()
    {
        return lexeme;
    }

    public LexemeType getType()
    {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lexeme lexeme1 = (Lexeme) o;

        if (!lexeme.equals(lexeme1.lexeme)) return false;
        return type == lexeme1.type;

    }

    @Override
    public int hashCode() {
        int result = lexeme.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }

    public enum LexemeType
    {
        OPENING_BRACKET,
        CLOSING_BRACKET,
        DIV_SIGN,
        MUL_SIGN,
        PLUS,
        MINUS,
        DEGREE,
        NUMBER,
        EOF
    }
}
