package main.java.com.sohich.mtc;



import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class Lexer
{
    private Reader reader;
    private final int BUF_SIZE = 1;
    private String fragement = "";
    private boolean lexemeUnproceed;
    private boolean readingNumber = false;
    private boolean metLexeme = false;
 //   private Lexeme

    public Lexer(Reader reader)
    {
        this.reader = reader;
    }

    public Lexeme getLexeme() throws IOException, LexerException
    {
        char[] buffer = new char[BUF_SIZE];
        int bytesRead = 0;
        metLexeme = false;

        do
        {
            if(!lexemeUnproceed)
            {
                do {
                    bytesRead = reader.read(buffer, 0, BUF_SIZE);
                }
                while(Character.isSpaceChar(buffer[0]) && !metLexeme);
            }
            else
            {
                buffer[0] = fragement.charAt(0);
            }
            lexemeUnproceed = false;

            if(bytesRead == -1)
            {
                if(readingNumber)
                {
                    readingNumber = false;
                    return new Lexeme(fragement, Lexeme.LexemeType.NUMBER);
                }
                return new Lexeme("", Lexeme.LexemeType.EOF);
            }
            switch (buffer[0])
            {
                case ' ':
                    if(readingNumber)
                    {
                        readingNumber = false;
                        String number = fragement;
                        fragement = "";
                        return new Lexeme(number, Lexeme.LexemeType.NUMBER);
                    }
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    readingNumber = true;
                    metLexeme = true;
                    fragement += buffer[0];
                    break;
                case '+':
                    if(readingNumber)
                    {
                        return new Lexeme(flushLexeme(buffer[0]), Lexeme.LexemeType.NUMBER);
                    }
                    fragement = "";
                    return new Lexeme("" + buffer[0], Lexeme.LexemeType.PLUS);
                case '-':
                    if(readingNumber)
                    {
                        return new Lexeme(flushLexeme(buffer[0]), Lexeme.LexemeType.NUMBER);
                    }
                    fragement = "";
                    return new Lexeme("" + buffer[0], Lexeme.LexemeType.MINUS);
                case '*':
                    if(readingNumber)
                    {
                        return new Lexeme(flushLexeme(buffer[0]), Lexeme.LexemeType.NUMBER);
                    }
                    fragement = "";
                    return new Lexeme("" + buffer[0], Lexeme.LexemeType.MUL_SIGN);
                case '/':
                    if(readingNumber)
                    {
                        return new Lexeme(flushLexeme(buffer[0]), Lexeme.LexemeType.NUMBER);
                    }
                    fragement = "";
                    return new Lexeme("" + buffer[0], Lexeme.LexemeType.DIV_SIGN);
                case '^':
                    if(readingNumber)
                    {
                        return new Lexeme(flushLexeme(buffer[0]), Lexeme.LexemeType.NUMBER);
                    }
                    fragement = "";
                    return new Lexeme("" + buffer[0], Lexeme.LexemeType.DEGREE);
                case '(':
                    if(readingNumber)
                    {
                        return new Lexeme(flushLexeme(buffer[0]), Lexeme.LexemeType.NUMBER);
                    }
                    fragement = "";
                    return new Lexeme("" + buffer[0], Lexeme.LexemeType.OPENING_BRACKET);
                case ')':
                    if(readingNumber)
                    {
                        return new Lexeme(flushLexeme(buffer[0]), Lexeme.LexemeType.NUMBER);
                    }
                    fragement = "";
                    return new Lexeme("" + buffer[0], Lexeme.LexemeType.CLOSING_BRACKET);
                default:
                    throw new LexerException("Unknown symbol");
            }
        }
        while(Character.isDigit(buffer[0]) );

        return new Lexeme(fragement, Lexeme.LexemeType.NUMBER);
    }

    private String flushLexeme(char sign)
    {
        readingNumber = false;
        String number = fragement;
        fragement = "" + sign;
        lexemeUnproceed = true;
        return number;
    }

}