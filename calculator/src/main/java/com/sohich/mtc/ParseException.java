package main.java.com.sohich.mtc;

public class ParseException extends Exception
{
    public ParseException()
    {
        super();
    }

    public ParseException(String reason)
    {
        super(reason);
    }

}
