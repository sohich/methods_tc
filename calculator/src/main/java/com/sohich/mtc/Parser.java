package main.java.com.sohich.mtc;
import java.io.IOException;
import java.io.StringReader;

public class Parser
{
    private Lexer lexer;
    private Lexeme nextLexeme;

    public Parser(Lexer lexer)
    {
        this.lexer = lexer;
    }

    public int calculate() throws IOException, LexerException, ParseException
    {
        nextLexeme = lexer.getLexeme();
        int result = parseExpression();
        if(nextLexeme.getType() != Lexeme.LexemeType.EOF)
        {
            throw new ParseException("Unexpected expression containing");
        }
        return result;
    }

    private int parseExpression() throws LexerException, IOException, ParseException
    {
        int result = parseTerm();
        int sign = 1;
        while(nextLexeme.getType() == Lexeme.LexemeType.PLUS
           || nextLexeme.getType() == Lexeme.LexemeType.MINUS)
        {
            if(nextLexeme.getType() == Lexeme.LexemeType.PLUS)
            {
                sign = 1;
            }
            else if(nextLexeme.getType() == Lexeme.LexemeType.MINUS)
            {
                sign = -1;
            }
            // error handling here
            nextLexeme = lexer.getLexeme();
            result += sign * parseTerm();
        }
        return result;
    }

    private int parseTerm() throws LexerException, IOException, ParseException
    {
        int result = parseFactor();
        while(nextLexeme.getType() == Lexeme.LexemeType.MUL_SIGN
           || nextLexeme.getType() == Lexeme.LexemeType.DIV_SIGN)
        {

            switch (nextLexeme.getType())
            {
                case MUL_SIGN:
                    nextLexeme = lexer.getLexeme();
                    result *= parseFactor();
                    break;
                case DIV_SIGN:
                    nextLexeme = lexer.getLexeme();
                    result /= parseFactor();
                    break;
            }
        }
        return result;
    }

    private int parseFactor() throws LexerException, IOException, ParseException
    {
        int result = parsePower();
        if(nextLexeme.getType() == Lexeme.LexemeType.DEGREE)
        {
            nextLexeme = lexer.getLexeme();
            result = power(result, parseFactor());
        }
        return result;
    }

    private int parsePower() throws LexerException, IOException, ParseException
    {
        int result;
        int sign = 1;
        if(nextLexeme.getType() == Lexeme.LexemeType.MINUS)
        {
            sign = -1;
            nextLexeme = lexer.getLexeme();
        }

        result = parseAtom() * sign;
        return result;
    }

    private int parseAtom() throws LexerException, IOException, ParseException
    {
        int result;
        if(nextLexeme.getType() == Lexeme.LexemeType.OPENING_BRACKET)
        {
            nextLexeme = lexer.getLexeme();
            result = parseExpression();

            if(nextLexeme.getType() != Lexeme.LexemeType.CLOSING_BRACKET)
            {
                throw new ParseException("Closing bracket expected");
            }
        }
        else if(nextLexeme.getType() == Lexeme.LexemeType.NUMBER)
        {
            result = Integer.parseInt(nextLexeme.getLexeme());
        }
        else
        {
            throw new ParseException("Unexpected atom");
        }
        nextLexeme = lexer.getLexeme();
        return result;
    }

    private int power(int a, int b) throws ParseException
    {
        int power = 1;
        if(b < 0)
        {
            if(a == 0)
            {
                throw new ParseException("Could not calculate zero in negative degree");
            }
            return 0;
        }
        for(int c = 0; c < b; c++)
        {
            power *= a;
        }
        return power;
    }
}
