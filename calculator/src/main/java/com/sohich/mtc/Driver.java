package main.java.com.sohich.mtc;

import java.io.*;

public class Driver
{
    public static void main(String[] args) {
        if(args.length != 1)
        {
            System.out.println("Calculator!\nArgument required: input file location");
            System.exit(0);
        }
        try
        {
            Parser parser = new Parser(new Lexer(new BufferedReader(new FileReader(args[0]))));
            int result = parser.calculate();
            System.out.println(result);
        }
        catch(IOException | LexerException | ParseException e)
        {
            e.printStackTrace();
        }
    }
}
